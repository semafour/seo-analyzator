var _app = {
    context: '#ajax-content',
    check: {},
    init: function (cb) {
	$('body').append('<div id="ajax-content" class="hidden">*ajax-content*</div>');

	var _interval = setInterval(function () {
	    clearInterval(_interval);
	    if (typeof (cb) === 'function') {
		cb();
	    }
	}, 50);
    },
    flash: function (text, type, info) {
	if (typeof (type) === 'undefined' || type == null) {
	    type = 'default';
	}
	var _html = '<span class="flash-msg ' + type + '">' + text;
	if (typeof (info) != 'undefined') {
	    _html += '<i class="fa fa-info-circle"></i>';
	    _html += '<div class="flash-info hidden">' + (typeof (info) !== 'function' ? info : info()) + '</div>';
	}
	_html += '</span>';
	$('#message-content').append(_html);
    }
}


$(function () {

    _app.init(function () {
	$('.container .btn.btn-primary').trigger('click'); // automaticke spustenie bez potreby kliknut
    });

    $(document).on('click', '.flash-msg .fa-info-circle', function (e) {
	e.preventDefault();
	var _t = $(e.currentTarget).next();
	if (_t.hasClass('hidden')) {
	    _t.removeClass('hidden');
	}
	else {
	    _t.addClass('hidden');
	}
    });

    $(document).on('click', '.btn.btn-primary', '.container', function (e) {
	$('#ajax-spinner').show();
	$('.container').hide();

	chrome.tabs.query({active: true}, function (tabs) {
	    $.ajax({
		url: tabs[0].url,
		success: function (res) {
		    res = res.replace(/src=/gi, "data-src=");
		    res = res.replace(/href=/gi, "data-href=");
		    res = res.replace(/<script/gi, "<!--script");
		    res = res.replace(/\/script>/gi, "/script-->");

		    $(_app.context).html(res);
		    $('body').append('<div id="message-content"></div>');
		    $('#ajax-spinner').hide();
		    var _int = setInterval(function () {
			_app.check.init();
			clearInterval(_int);
		    }, 300);
		},
		timeout: 12000,
		error: function () {
		    $('#ajax-spinner').hide();
		    $('body').html('<p class="interrupt-error">Nastala neznáma chyba pri analýze stránky.</p>');
		}
	    });
	});
    });
});


_app.check = {
    init: function () {
	$('#message-content').html('');

	this.metaTags();
	this.h123();
	this.images();
    },
    metaTags: function () {
	// title ?
	if ($('title', _app.context).length == 1) {
	    _app.flash('Title stránky bol nájdený', null, $('title', _app.context).text());
	}
	else {
	    _app.flash('Title stránky sa nepodarilo nájsť', 'danger');
	}

	// kontrola meta description
	if ($('meta[name=description]', _app.context).length == 1) {
	    _app.flash('Meta description bolo nájdené', null, $('meta[name=description]', _app.context).attr('content'));
	}
	else {
	    _app.flash('Meta description sa nepodarilo nájsť', 'danger');
	}

	// kontrola keywords
	if ($('meta[name=description]', _app.context).length == 1) {
	    _app.flash('Meta keywords bolo nájdené', null, $('meta[name=keywords]', _app.context).attr('content'));
	} else {
	    _app.flash('Meta keywords sa nepodarilo nájsť');
	}

	// favicon
	var _favicon = false;
	$('link').each(function () {
	    if ($(this).attr('rel').match(/icon/im)) {
		_favicon = true;
	    }
	});
	if (_favicon) {
	    _app.flash('Facivon bol nájdený.');
	}
	else {
	    _app.flash('Facivon sa nepodarilo nájsť.', 'danger');
	}

	// optimalizacia pre facebook
	var _fb = false;
	$('meta[property]').each(function () {
	    if ($(this).attr('property').match(/og:[a-z]{3,}/i)) {
		_fb = true;
	    }
	});
	if (_fb) {
	    _app.flash('Optimalizácia pre Facebook bola nájdená.', null, function () {
		var _str = '';
		$('meta[property]').each(function () {
		    if ($(this).attr('property').match(/og:[a-z]{3,}/i)) {
			_str += '- ' + $(this).attr('property').replace('og:', '') + ': ' + $(this).attr('content') + '<br />';
		    }
		});
		return _str;
	    });
	}
	else {
	    _app.flash('Optimalizáciu pre Facebook sa nepodarilo nájsť.', 'danger');
	}

	// optimalizacia pre twitter
	var _tw = false;
	$('meta[name]').each(function () {
	    if ($(this).attr('name').match(/twitter:[a-z]{3,}/i)) {
		_tw = true;
	    }
	});
	if (_tw) {
	    _app.flash('Optimalizácia pre Twitter bola nájdená.', null, function () {
		var _str = '';
		$('meta[name]').each(function () {
		    if ($(this).attr('name').match(/twitter:[a-z]{3,}/i)) {
			_str += '- ' + $(this).attr('name').replace('twitter:', '') + ': ' + $(this).attr('content') + '<br />';
		    }
		});
		return _str;
	    });
	}
	else {
	    _app.flash('Optimalizáciu pre Twitter sa nepodarilo nájsť.');
	}
    },
    h123: function () {
	// kontrola H1 tagov
	if ($('h1', _app.context).length == 1) {
	    _app.flash('Nadpis H1 bol nájdený.', null, function () {
		return '- ' + $('h1', _app.context).text();
	    });
	}
	else if ($('h1', _app.context).length > 1) {
	    _app.flash('Bolo nájdených viacero H1 nadpisov', 'danger', function () {
		var _str = '';
		$('h1', _app.context).each(function () {
		    _str += '- ' + $(this).text() + '<br />';
		});
		return _str;
	    });
	}
	else {
	    _app.flash('Nadpis H1 sa nepodarilo nájsť.', 'danger');
	}

	// kontrola H2 tagov
	if ($('h2', _app.context).length >= 1) {
	    _app.flash('Zoznam nadpisov H2', null, function () {
		var _str = '';
		$('h2', _app.context).each(function () {
		    _str += '- ' + $(this).text() + '<br />';
		});
		return _str;
	    });
	}
	else {
	    _app.flash('Nepodarilo sa nájsť žiaden H2 nadpis.');
	}

	// kontrola H3 tagov
	if ($('h3', _app.context).length >= 1) {
	    _app.flash('Zoznam nadpisov H3', null, function () {
		var _str = '';
		$('h3', _app.context).each(function () {
		    _str += '- ' + $(this).text() + '<br />';
		});
		return _str;
	    });
	}
    },
    images: function () {
	
	// kontrola obrazkov bez alt textov + pocet obrazkov
	if ($('img', _app.context).length) {
	    _app.flash('Našlo sa ' + $('img', _app.context).length + ' IMG tagov.', null);

	    var _wihoutImage = '';
	    $('img', _app.context).each(function () {
		if( !$(this).attr('alt') || !$(this).attr('alt').length ){
		    _wihoutImage += (_wihoutImage.length ? '<br />':'') + '- ' + $(this).attr('data-src');
		}
	    });
	    
	    if( _wihoutImage.length ){
		_app.flash('Našli sa IMG tagy bez vyplnených ALT-ov.','danger',_wihoutImage);
	    }
	}
	else {
	    _app.flash('Nepodarilo sa nájsť žiaden IMG tag.', null);
	}
    }
};